//Map for BnbClass
var nodeMap = {
    "A" : ["B", "I", "L"],
    "B" : ["A", "C"],
    "C" : ["B", "D", "G"],
    "D" : ["C", "E"],
    "E" : ["D", "F"],
    "F" : ["E", "H"],
    "G" : ["C"],  
    "H" : ["J", "F"],
    "I" : ["A"],  
    "J" : ["H", "K"],
    "K" : ["J", "L"],
    "L" : ["K", "A"]
};

//Coordinates Map for ui
var uiMap = {
	"A": [100, 400],
	"B": [200, 400],
	"C": [300, 400],
	"D": [300, 300],
	"E": [300, 200],
	"F": [300, 100],
	"G": [400, 400],
	"H": [200, 100],
	"I": [100, 500],
	"J": [100, 100],
	"K": [100, 200],
	"L": [100, 300]
};

var counter = 0;
var bnb = new BnbClass(nodeMap, "I", "G"); //Create Object
var demonstrationVariables =  bnb.demonstrate();
var pathToFollow = createPathForUi(demonstrationVariables.finalCases);

function setup() {
	createCanvas(500, 600);
	background(220);

	createSquares(); // Create empty squares
	initialize(); // Create maze (grey collored squares)
	frameRate(5); // Initialize frames
}

// This functions loops according to p5.js documentation
function draw() {
	fill(250, 100, 200) // Set color
  
	rect(uiMap[pathToFollow[counter]][0], uiMap[pathToFollow[counter]][1], 100, 100) // Mark rectangle
	
	// If we arrive at the next-to-last node
    if(uiMap[pathToFollow[counter]][0] == 300 && uiMap[pathToFollow[counter]][1] == 400) {
        rect(uiMap['G'][0], uiMap['G'][1], 100, 100) // Mark final node
        if(counter >= pathToFollow.length - 2) { // if we are at the end of the pathToFollow (which means the final best case)
			noLoop();
			document.getElementById("table").innerHTML = createTable();
        }
    }
	
	// If we arrive at a final state, initialize maze.
    if(uiMap[pathToFollow[counter]][0] == 400 && uiMap[pathToFollow[counter]][1] == 400) { //if the coordinates match.
        initialize();
    }
  	counter++;
}

// Creates ui path of the maze
function initialize() {
	fill(100)
	for (i in uiMap) {
		rect(uiMap[i][0], uiMap[i][1], 100, 100)
	}
}

// Create empty squares
function createSquares() {
	for (y = 0; y < height; y += 100) {
		for (x = 0; x < width; x += 100) {
			fill(60); // set color
			rect(x, y, 100, 100);
		}
	}
}

// Creates info table
function createTable() {
	var html = "<table>";
	for(element in demonstrationVariables.states) { //foreach element in states
		html += "<tr>";
		for (key in demonstrationVariables) {
			if(demonstrationVariables[key] != demonstrationVariables.finalCases) { //finalCases is used for the ui, not for the table
				html += "<td>" + demonstrationVariables[key][element] + "</td>";
			}
		}
		html += "</tr>"
	}
	html += "</table>";
	return html;
}

// Takes the finalStates array and makes it a string
function createPathForUi(finalStates) {
	var pathToFollow = "";
	finalStates.forEach(element => {
		pathToFollow = pathToFollow + element;
	});
	return pathToFollow;
}