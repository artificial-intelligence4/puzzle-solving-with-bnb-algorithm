class BnbClass {

    constructor(map, initialNode, finalNode) { 
		this._myMap = map;
        this._initialNode = initialNode;
		this._finalNode = finalNode;
		this._frontier = [initialNode]; //Initial State. Frontier is used as a stack.
		this._closedStates = ["Closed States", ""]; 
		this._bestCost = Number.MAX_SAFE_INTEGER;
		// Demonstration variables start
		this._states = ["States"]; // For ui.
		this._kids = ["Children"]; // Kids array is used for the ui.
		this._finalCases = []; // This list is used for the ui representation.
	}

	// Bnb algorithm. Returns bestState
	bnbAlgorithm() {
		var currentState;
		var currentCost;
		while(!this.isEmpty(this._frontier)) {
			currentState = this._frontier.pop();
			currentCost = this.cost(currentState);
			if(this.prune(currentState) && this.checkClosedStates(currentState)) {
				this._states.push(currentState); // For demonstration. Keep record of the states
				if(this.checkIfFinalState(currentState)) {
					this._bestState = currentState;
					this._bestCost = currentCost;
					this._kids.push("Final Case. Best Cost: " + this._bestCost); // For demonstration. Mark final case.
					this._finalCases.push(this._bestState); // For demonstration. Keep record of the final states.
				}
				else {
					this.insertIntoFrontier(this.children(currentState));
					this._kids.push(this.children(currentState)); // For demonstration. Keep record of the kids
					console.log("Frontier: " + this._frontier); // For demonstration. Print frontier to console.
				}
				this._closedStates.push(currentState);
			}
		}
		return this._bestState == null ? null : this._bestState;
	}

	//Runs bnbAlgorithm and returns demonstrationVariables
	demonstrate() {
		this.bnbAlgorithm();
		return {
			closedStates : this._closedStates,
			states : this._states,
			kids : this._kids,
			finalCases : this._finalCases
		}
	}

	//Generates children based on current state
	children(currentState) {
		var canditateKids = this._myMap[this.getLastElement(currentState)]; // Get children from map
		var newStates = [];
		//for each canditate
		canditateKids.forEach(element => {
			if(this.checkIfExistsInPath(currentState, element)) { //if it doesn't exists in the path
				newStates.push(currentState + element);
			}
		});
		return newStates;
	}
	
	//Insert children in the frontier
	insertIntoFrontier(childrenStates) {
		childrenStates.forEach(element => {
			this._frontier.push(element);
		});
	}
	
	//Checks if a state exists in the closedStates
	checkClosedStates(state) {
		return this._closedStates.indexOf(state) == -1 ? true : false; //if it doesn't exist, return true
	}
	
	checkIfFinalState(state) {
		return this.getLastElement(state) == this._finalNode ? true : false;
	}
	
	//Returns false if best cost is less than the cost of the state
	prune(state) {
		return this._bestCost > this.cost(state) ? true : false; 
	}
	
	//properties/ tools
	getLastElement(array) {
		return array.length ? array[array.length - 1] : (void 0); //if false, returns undentified
	}
	
	isEmpty(array) {
		return array.length == 0 ? true : false;
	}
		
	cost(state) {
		return state.length;
	}

	//Checks if one of the kids already exist in the path.
	checkIfExistsInPath(path, element) {
		return path.indexOf(element) == -1 ? true : false;
	}
}