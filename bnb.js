var myMap = {
    "A" : ["B", "I", "L"],
    "B" : ["A", "C"],
    "C" : ["B", "D", "G"],
    "D" : ["C", "E"],
    "E" : ["D", "F"],
    "F" : ["E", "H"],
    "G" : ["C"],  
    "H" : ["J", "F"],
    "I" : ["A"],  
    "J" : ["H", "K"],
    "K" : ["J", "L"],
    "L" : ["K", "A"]
};

//will be initialized in constructor //////////////////////////////
var initialNode = "I"
var finalState = "G";
var frontier = [initialNode]; //Initial State. Frontier is used as a stack.
var closedStates = []; 
var states = [];
var bestState;
var bestCost = 5000;
var currentState;
var currentCost;


function bnbAlgorithm() {
    console.log(!isEmpty(frontier))
    while(!isEmpty(frontier)) {
        currentState = frontier.pop();
        currentCost = cost(currentState);
        if(prune(currentState) && checkClosedStates(currentState)) {
            if(checkIfFinalState(currentState)) {
                bestState = currentState;
                bestCost = currentCost;
                console.log(bestCost);
            }
            else {
                insertIntoFrontier(children(currentState));
                console.log(frontier);
            }
            closedStates.push(currentState);
        }
        console.log(bestState); ///////////
    }
    ///////////////////
}

function children(currentState) {
    var canditateKids = myMap[getLastElement(currentState)];
    var newStates = [];
    //for each canditate
    canditateKids.forEach(element => {
        if(checkIfExistsInPath(currentState, element)) { //if it doesn't exists in the path
            newStates.push(currentState + element);
        }
    });
    return newStates;
}


function insertIntoFrontier(childrenStates) {
    childrenStates.forEach(element => {
        frontier.push(element);
    });
}

//Checks if one of the kids already exist in the path.
function checkIfExistsInPath(path, element) {
    return path.indexOf(element) == -1 ? true : false;
}

//Checks if a state exists in the closedStates
function checkClosedStates(state) {
    return closedStates.indexOf(state) == -1 ? true : false; //if it doesn't exist, return true
}

function checkIfFinalState(state) {
    return getLastElement(state) == finalState ? true : false;
}

function cost(state) {
    return state.length;
}

function prune(state) {
    return bestCost > state.length ? true : false; 
}

//properties/ tools
function getLastElement(array) {
    return array.length ? array[array.length - 1] : (void 0); //if false, returns unudentified
}

function isEmpty(array) {
    return array.length == 0 ? true : false;
}

bnbAlgorithm();